DISM /online /enable-feature /featurename:DirectPlay /all

netsh advfirewall firewall add rule name="Age of Empires II Expansion" dir=in action=allow program="C:\intel\age2_x2.2\age2_x1\age2_x2.exe" enable=yes protocol=TCP
netsh advfirewall firewall add rule name="Age of Empires II Expansion" dir=in action=allow program="C:\intel\age2_x2.2\age2_x1\age2_x2.exe" enable=yes protocol=UDP
netsh advfirewall firewall add rule name="Microsoft DirectPlay Helper" dir=in action=allow program="C:\windows\syswow64\dplaysvr.exe" enable=yes protocol=TCP
netsh advfirewall firewall add rule name="Microsoft DirectPlay Helper" dir=in action=allow program="C:\windows\syswow64\dplaysvr.exe" enable=yes protocol=UDP
